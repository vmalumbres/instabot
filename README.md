
# InstaBot

Development and testing of differents ig bots

## Repo

    |- README.md
    |- LICENSE
    |- src

## Appendix
	
```
It's needed to add to $PATH the directory of geckodriver. By running:

export PATH=$PATH:[directory-to-gecko-driver]

For downloading geckodriver, check:  https://github.com/mozilla/geckodriver/releases/download/v0.32.0/geckodriver-v0.32.0-linux-aarch64.tar.gz
```
